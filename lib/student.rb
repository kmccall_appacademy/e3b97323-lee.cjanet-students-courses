class Student
  attr_reader :first_name, :last_name, :course_load
  attr_accessor :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
    @course_load = Hash.new(0)
  end

  def name
    @first_name + " " + @last_name
  end

  def enroll(course)
    course_conflicts(course)
    @courses << course if !@courses.include?(course)
    course.students << self if !course.students.include?(self)
  end

  def course_conflicts(course)
    @courses.each {|current_course| raise "Conflicted schedule" if current_course.conflicts_with?(course)}
  end


  def push(*classes)
    classes.each do |course, dept, credit|
      enroll(course)
      credits(dept, credit)
    end
  end


  def credits(dept, credit)
    @course_load[dept]+=credit
  end

end
